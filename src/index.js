import axios from 'axios';
import { init } from '@sentry/browser';

init({
  dsn: process.env.SENTRY,
});

/* eslint-disable no-undef,no-console */
function log(...args) {
  if (process.env.DEBUG) {
    console.log(args);
  }
}

let checkLoggedInterval = {};
let shouldReload = false;

const getCookie = (cookies = '', name) => {
  log('trying to get auth cookie from', cookies);

  if (!cookies) return null;

  const match = cookies.match(new RegExp(`(^| )${name}=([^;]+)`));

  log('we got match', match);

  if (match) return match[2];

  return null;
};

const getAuthToken = (browser, cb) => {
  log('trying to get token');

  return browser.executeScript(
    { code: 'document.cookie' },
    (cookies) => {
      if (!cookies || !cookies[0] || !cookies[0].includes('AUTH_INSPECTOR')) {
        log('no cookies', cookies);
        return null;
      }

      return cb(getCookie(cookies[0], 'AUTH_INSPECTOR'));
    },
  );
};


function registerDevice({
  uuid,
  manufacturer,
  registrationId,
  token,
}) {
  const deviceType = manufacturer === 'Apple' ? manufacturer : 'Google';
  const params = {
    device_type: deviceType,
    device_id: uuid,
    device_token: registrationId,
  };

  const options = {
    headers: {
      'X-jwt-Token': token,
    },
  };

  return axios.post(
    `${process.env.API_URL}/api/v1/device`,
    params,
    options,
  ).then((resp) => {
    log(resp);
    clearInterval(checkLoggedInterval);
  }).catch((e) => {
    log('Exception', e);

    clearInterval(checkLoggedInterval);
    checkLoggedInterval = setInterval(authGetter, 60000);
  });
}

const App = {
  browser: {},
  popBrowser: {},
};

const IABEvents = {
  LOAD_ERROR: 'loaderror',
  LOAD_START: 'loadstart',
  LOAD_COMPLETE: 'loadstop',
};

const pusherConfig = {
  android: {
    icon: 'status',
    color: '#139e5d',
    iconColor: '#139e5d',
  },
  browser: {},
  ios: {
    alert: 'true',
    badge: 'true',
    sound: 'true',
  },
};

// sample data
// {   message: "actual message here",
//     additionalData: {foreground: true, coldstart: false, url: "/cars/running", color: "red"}
// }
function initPushes(token) {
  if (!token) return;

  log('we got token', token);

  const push = PushNotification.init(pusherConfig);

  push.on('registration', (data) => {
    const { uuid, manufacturer } = device;

    registerDevice({
      registrationId: data.registrationId,
      uuid,
      manufacturer,
      token,
    }).then(resp => log('device registered', resp));
  });

  push.on('notification', ({ additionalData, message }) => {
    const {
      url, color, foreground,
    } = additionalData;

    let code = `window.$nuxt.$store.commit("setError", {
            message: "${message}",
            url: "${url}",
            color: "${color || 'primary'}",
            shouldRedirect: true
        });`;

    if (url && !foreground) {
      code = `window.$nuxt.$router.push("${url}")`;
    }

    App.browser.executeScript({
      code,
    }, () => { });

    log(code);
  });
}

function onLoadError(params) {
  log(params);
  const { code } = params;
  if (code !== -1003) { return; }

  App.browser = window.open(
    process.env.URL_OLD,
    process.env.URL_TARGET,
    process.env.URL_OPTIONS,
  );

  App.browser.removeEventListener(
    IABEvents.LOAD_ERROR,
    onLoadError,
  );
}

function disableZoom() {
  const code = `const metaTags = document.getElementsByName('viewport');
      metaTags.forEach((metaTag) => {
        metaTag.setAttribute(
          'content', 
          'width=device-width, initial-scale=1.0, user-scalable=no'
        );
      });`;
  App.browser.executeScript({
    code,
  }, () => { });
}

function showRegistration() {
  const code = 'document.getElementById("registration").style.display = "block";';
  App.browser.executeScript({
    code,
  }, () => { });
}

// for cases where user traverses and the loadcomplete is not triggered,
// because of SPA implementation, event listener will be attached once.
function addClickListener() {
  const code = `
        function onClick() {
          const route = window.$nuxt.$route.name;
                  
          if (route == 'login') {
            document.getElementById('registration').style.display = 'block';
          }

          document.getElementsByClassName('footer')[0].style.display = 'none';
        }
  
        if (document.body) {
          document.body.addEventListener(
            'click', 
            () => { setTimeout(() => onClick(), 2000) }, false
          ); 
        }`;
  App.browser.executeScript({
    code,
  }, () => { });
}

function onLoadComplete(params) {
  log(params);

  addClickListener();

  const { url } = params;

  disableZoom();

  if (url.includes('login')) {
    showRegistration();
    return;
  }
  const code = '.v-footer { display: none; };';
  App.browser.insertCSS({ code });
}

function onLoadStart(params) {
  log(params);
  const { url } = params;

  if (url.indexOf(process.env.DOMAIN) === -1 || url.includes('.pdf')) {
    App.popBrowser = window.open(url, '_system');
    shouldReload = true;
  }
}

function addEventListenersToIAB() {
  App.browser.addEventListener(
    IABEvents.LOAD_ERROR,
    onLoadError,
  );
  App.browser.addEventListener(
    IABEvents.LOAD_COMPLETE,
    onLoadComplete,
  );
  App.browser.addEventListener(
    IABEvents.LOAD_START,
    onLoadStart,
  );
}

function onDeviceReady() {
  App.browser = window.open(
    process.env.URL,
    process.env.URL_TARGET,
    process.env.URL_OPTIONS,
  );

  checkLoggedInterval = setInterval(authGetter, 3000);
  cordova.plugins.notification.badge.configure({ autoClear: true });
  addEventListenersToIAB();
}

function onResume() {
  if (shouldReload) {
    onDeviceReady();
    shouldReload = false;
  }
}

const authGetter = () => getAuthToken(App.browser, initPushes);

document.addEventListener('deviceready', () => onDeviceReady(), false);
document.addEventListener('resume', () => onResume(), false);
